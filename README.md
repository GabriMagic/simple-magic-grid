# Simple Magic Grid

Simple Magic Grid is a versatile and intuitive CSS library designed to streamline the process of creating responsive grid layouts. It's a simple grid system, independent of any other framework, specializing solely in organizing the layout into columns. Built upon a flexible 12-column grid system, Simple Magic Grid empowers developers to craft dynamic and visually appealing designs for a wide range of applications.

## Key Features

- **Responsive Design:** Simple Magic Grid seamlessly adapts to various screen sizes and resolutions, ensuring optimal viewing experiences across devices, from mobile phones to desktop monitors.
- **Multiple Resolution Support:** With support for multiple resolutions, including small (s), medium (m), large (l), extra-large (xl), and more, Simple Magic Grid enables precise control over layout appearance on different devices.

- **Customizable Column Widths:** Developers have the flexibility to define custom column widths using intuitive class names, such as `grid-X`, where X represents the desired number of columns (from 1 to 12), allowing for granular control over layout structure.

- **Effortless Integration:** Integration with Simple Magic Grid is straightforward, whether through npm installation for seamless integration into Node.js projects or direct inclusion via CDN for quick implementation in web-based applications.

## Installation

You can easily integrate Simple Magic Grid into your project using npm:

```
npm install simple-magic-grid
```

Alternatively, you can include Simple Magic Grid directly into your HTML files using a CDN:

```html
<link
  rel="stylesheet"
  href="node_modules/simple-magic-grid/dist/simple-magic-grid.min.css"
/>
```

## Usage

To harness the power of Simple Magic Grid, simply apply the `magic-grid` class to your container element and utilize the provided grid classes to define layout structures tailored to your requirements.

### Example

Here's an example demonstrating the usage of Simple Magic Grid for creating responsive layouts:

```html
<div class="magic-grid">
  <div class="grid-12 grid-s-12 grid-m-6 grid-l-4">
    Full width on small screens, half width on medium screens, and one-third
    width on large screens
  </div>
</div>
```
